import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = 'URL_API'; 

  constructor(private http: HttpClient) { }

  submitForm(formData: any) {
    return this.http.post(this.apiUrl + '/submit-form', formData);
  }
}
