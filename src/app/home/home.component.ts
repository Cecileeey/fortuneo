import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import { ApiService } from './api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(   
    private apiService: ApiService,
    private formBuilder: FormBuilder) {
  }
  public form! : FormGroup;
  @Input() data:any;
  @Input() sourceForm: any;

 ngOnInit() : void {
   this.form = new FormGroup({
     duree: new FormControl('', Validators.required),
     campagne: new FormControl('', Validators.required),
     pdays: new FormControl('', Validators.required),
     housing: new FormControl('', Validators.required),
     poutcome: new FormControl('', Validators.required)
   })
 }

  submit() : void {
    //this.form.value;
    if (this.form.valid) {
      const formData = this.form.value;
      this.apiService.submitForm(formData).subscribe(
        response => {
          console.log('Réponse Api:', response);
        },
        error => {
          console.error('Erreur lors de la requête API:', error);
        }
      );
    }else {
      console.warn('Le formulaire n\'est pas encore initialisé.');
    }
  }

}
